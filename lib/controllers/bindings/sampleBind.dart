import 'package:get/get.dart';


import '../sumController.dart';

class SampleBind extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SumController>(() => SumController());
  }
}
