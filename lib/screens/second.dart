import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:getx/controllers/sumController.dart';


class Second extends GetWidget {
  @override
  Widget build(BuildContext context) {
    return GetX<SumController>(

      builder: (controller)
      {

        return Scaffold(
          appBar: AppBar(
            title: Text('second Route'),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Counter #1:    ${controller.count1.value}',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),

                Text("                        +"),
                Text(
                  'Counter #2:    ${controller.count2.value}',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),

                Text("                        ="),
                Text(
                  'Sum:                 ${controller.sum}',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),

                SizedBox(
                  height: 40,
                ),
                RaisedButton(
                  child: Text("Increment Counter #1"),
                  onPressed: () {
                    controller.increment();
                  },
                ),
                RaisedButton(
                  child: Text("Increment Counter #2"),
                  onPressed: () {
                    controller.increment2();
                  },
                ),
                RaisedButton(
                  child: Text("Store Values"),
                  onPressed: () {
                    GetStorage box = GetStorage();
                    box.write("count1", controller.count1.value);
                    box.write("count2", controller.count2.value);
                  },
                ),
                SizedBox(
                  height: 40,
                ),
                RaisedButton(
                  child: Text("Change Theme"),
                  onPressed: () {
                    if (Get.isDarkMode) {
                      Get.changeTheme(ThemeData.light());
                    } else {
                      Get.changeTheme(ThemeData.dark());
                    }

                    print("Screen Height: " + Get.height.toString());
                    print("Screen Width: " + Get.width.toString());
                    print("Is Device IOS?: " + GetPlatform.isIOS.toString());
                    print(
                        "Is Device Android?: " + GetPlatform.isAndroid.toString());
                  },
                ),
              ],
            ),
          ),
        );

      },
    );
  }
}
